## Requirements:
+ NodeJS installation. This has been developed using v8.9.5 LTS. Older version might work too.
+ Nodemon
## How to use:
`cd ../project_folder`

then run

`npm install`

and

`nodemon ./server.js
`

Visit the site by going to `localhost:8080/index`

## How '/books' route works:
  + When client makes a request to '/books' route, the server will begin to read in 4 text files. This read operation is
  asynchronous with the help of the built in Promise library. When a text file finish reading, it will be pushed into an array.
  + Since these asychronous read operations are non blocking, whatever read operation finished early will be pushed into the array early.
   This will lead to a 'random' order of the text data that populated the array. The server then wait for 5 seconds to begin to send the array 
   that contains these text back to the client. The result is a webpage that displays the read text files in a seemingly random order.