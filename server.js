var http = require('http');
var fs = require('fs');
var url = require('url');
var path = require('path');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
const express = require('express');
const app = express();
var Q = require('q');


app.set('view engine', 'ejs');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static('public'));

app.get('/index*', function(req, res) {
    res.cookie('last-job', 'adventurer');    
    res.cookie('injury', 'arrow to the knee');
    res.cookie('current-job', 'guard duty');
    res.render(path.join(__dirname + '/view/index'), {headers: JSON.stringify(req.headers, null, 2)});
});

app.post('/index', (req, res) => {
    res.render(path.join(__dirname + '/view/index'), {headers: JSON.stringify(req.headers, null, 2), 
                                                        cookies: JSON.stringify(req.cookies, null, 2),
                                                        email: req.body.email,
                                                        password: req.body.password,
                                                        race:req.body.race,
                                                        specialty: req.body.specialty});
});

app.get('/books', (req, res) => {
    var readFileQ = Q.denodeify(fs.readFile)
    var promise = readFileQ('./public/text-1.txt', 'utf8');
    var books = []

    // read text file 1 to 4 in order
    promise.then( function (content) {
        books.push({book: 1, text: content});
    });
    promise = readFileQ('./public/text-2.txt', 'utf8').then(function (content){
        // WITH timeout
        // setTimeout( () => books.push({book: 2, text: content}), 3000);
        // WITHOUT timeout
        books.push({book: 2, text: content});
    });
    promise = readFileQ('./public/text-3.txt', 'utf8').then(function (content){
        books.push({book: 3, text: content});
    });
    promise = readFileQ('./public/text-4.txt', 'utf8').then(function (content){
        books.push({book: 4, text: content});
    });
    // wait 5 seconds before sending response to make sure all text files have been read and append to array
    setTimeout( () => res.render(path.join(__dirname + '/view/books'), {books:books}), 5000);   

});

app.get('/login', function(req, res) {    
    // res.cookie('my-first-cookie', 'yay cookie');
    // res.cookie('my-second-cookie', 'oh more cookie? why not');
    // res.cookie('my-third-cookie', "We don't need no diabetes");
    res.sendFile(path.join(__dirname + '/html/login.html'));
    console.log()
});

app.post('/login', (req, res) => {
    let ip = req.ip;
    let method = req.method
    let url = req.url;
    let params = req.params;
    let path = req.path;
    let route = req.route;
    let toClient = {
        'ip': ip,
        'method': method,
        'url': url,
        'params': params,
        'path': path,
    };
    res.send();
});

app.get('/', function(req, res){
    let ip = req.ip;
    let method = req.method
    let url = req.url;
    let params = req.params;
    let path = req.path;
    let route = req.route;
    let toClient = {
        'ip': ip,
        'method': method,
        'url': url,
        'params': params,
        'path': path,
    };
    let htmlClient =
    "<html><head><style>table, th, td {border:1px solid black; border-collapse: collapse;}</style></head>" + 
    "<body>" + 
        "<table style='border:solid thin'>" +
            "<tr>" + 
                "<td>ip</td>" +
                "<td>" + ip + "</td>"+
            "</tr>" +

            "<tr>" + 
                "<td>method</td>" +
                "<td>" + method + "</td>"+
            "</tr>"+

            "<tr>" + 
                "<td>url</td>" +
                "<td>" + url + "</td>"+
            "</tr>" +
        "</table>" +
    "</body>" +
    "</html>"; 
    console.log(req.ip, method, url, params, path);
    
    res.send(htmlClient);
    
});


// Serving static html files
app.use(express.static('html'));

app.listen(8080, () => console.log('Express server on port 3000...'));

// http.createServer(function (req, res){
//     var reqHeaders = JSON.stringify(req.headers, null, 4);
//     var ipAddress = reqHeaders.connection;
//     console.log(req.url + " | " + req.method);
//     switch(req.url){
//         // index page
//         case '/index':
//         case '/index.html':
//             fs.readFile('index.html', function(err, data){
//                 res.writeHead(200, {'Content-type': 'text/html'});
//                 res.write(data);
//                 res.end();
//             });
//             break;
//         // submit url
//         case '/submit':
//             console.log(url.parse(req.url, true).query);
//             // res.write((url.parse(req.url, true).query, null, 4));
//             res.end();
//             break;
//         default:
//             res.write('\n-------HEADERS INFO--------\n');
//             res.write(JSON.stringify(req.headers, null, 4));
//             res.write('\n-------IP ADDRESS AND REFERER--------\n');
//             res.write(JSON.stringify({'ip': req.connection.remoteAddress, 'referer': req.url}));
//             res.end();                
//     }
// }).listen(8080);